package dao;

import com.cc.shop.dao.ProductDao;
import com.cc.shop.entity.Car;
import com.cc.shop.entity.Product;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import util.BaseTest;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class ProductDaoTest extends BaseTest {
    @Autowired
    private ProductDao productDao;

    @Test
    public void TestProductDao(){
        List<Product> list = productDao.findByVague("小米6");
        assertEquals(2,list.size());
    }

    @Test
    public void TestCheckInCarDao(){

        Car car = productDao.checkInCar("小米5S","admin");
        int n = car.getProductNum();
        assertEquals(1,n);
    }

}
