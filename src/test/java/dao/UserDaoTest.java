package dao;

import com.cc.shop.dao.UserDao;
import com.cc.shop.entity.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import util.BaseTest;

import static junit.framework.TestCase.assertEquals;

public class UserDaoTest extends BaseTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void TestUserDao(){
        User user = userDao.findByName("admin");
        assertEquals("admin",user.getPassword());
    }


}
