package com.cc.shop.serviceImpl;

import com.cc.shop.dao.ProductDao;
import com.cc.shop.entity.Car;
import com.cc.shop.entity.Product;
import com.cc.shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Override
    public List<Product> findByVague(String param) {
        return productDao.findByVague(param);
    }

    @Override
    public List<Product> findByName(String param) {
        return productDao.findByName(param);
    }

    @Override
    public void insertCar(String productName, Double productPrice, String productImg,int num,String username) {
        productDao.insertCar(productName,productPrice,productImg,num,username);
    }

    @Override
    public Car checkInCar(String productName,String username) {
        return productDao.checkInCar(productName,username);
    }

    @Override
    public void updateCar(String productName, Integer productNum,String username) {
//        先把原来数据取出来
        System.out.println("impl"+productName);
        System.out.println("impl"+productNum);
        System.out.println("impl"+username);
        int num = productNum + productDao.getNum(productName,username);
        productDao.updateCar(productName,num,username);
    }

//    分页处理
    @Override
    public List<Product> getAllProduct(Integer page, int limit) {
        return productDao.getProductByPage((page-1)*limit,limit);
    }

    //  获取总页数
    @Override
    public int getTotalProduct() {
        int total = productDao.getTotalProduct();
        int page = total/3;
        if (page<0)
            return 1;
        else
            return page;
    }

    @Override
    public boolean buyProduct(String productName, int num) {
        int exNum = productDao.getProduct(productName);
        int now = exNum - num;
        if (now<0)
            return false;
        else {
            productDao.upDateProductNum(now,productName);
            return true;
        }
    }


}
