package com.cc.shop.serviceImpl;

import com.cc.shop.dao.CarDao;
import com.cc.shop.entity.Car;
import com.cc.shop.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarDao carDao;

    @Override
    public List<Car> getAll(String username) {
        return carDao.getAllByName(username);
    }

    @Override
    public void payForProduct(String[] list) {
        carDao.delMore(list);
    }
}
