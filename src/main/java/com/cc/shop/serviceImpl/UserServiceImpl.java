package com.cc.shop.serviceImpl;

import com.cc.shop.dao.UserDao;
import com.cc.shop.entity.User;
import com.cc.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User Login(String username, String password) {
       return userDao.findByName(username);
    }

    @Override
    public User Check(String username) {
        User user = userDao.checkByUsername(username);
        if (user!=null){
            return user;
        }else {
            return null;
        }
    }

    @Override
    public void Regist(String username, String password) {
        userDao.addUser(username,password);
    }

}
