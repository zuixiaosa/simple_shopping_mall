package com.cc.shop.util;

import com.cc.shop.dto.Result;
import com.cc.shop.entity.User;

import javax.servlet.http.HttpSession;

public class CheckLogin {

    public boolean checkLogin(HttpSession session){
        User user;
        user = (User) session.getAttribute("user");
        if (user==null)
            return false;
        else
            return true;
    }
}
