package com.cc.shop.service;

import com.cc.shop.entity.User;


public interface UserService {

    User Login(String username, String password);

    User Check(String username);

    void Regist(String username, String password);
}
