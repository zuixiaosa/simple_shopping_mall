package com.cc.shop.service;

import com.cc.shop.entity.Car;
import com.cc.shop.entity.Param;
import com.cc.shop.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> findByVague(String param);

    List<Product> findByName(String param);

    void insertCar(String productName, Double productPrice, String productImg,int num,String username);

    Car checkInCar(String productName,String username);

    void updateCar(String productName, Integer productNum,String username);

    List<Product> getAllProduct(Integer page,int limit);

    int getTotalProduct();


    boolean buyProduct(String productName, int num);
}
