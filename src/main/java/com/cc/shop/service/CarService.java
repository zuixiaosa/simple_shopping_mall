package com.cc.shop.service;

import com.cc.shop.entity.Car;

import java.util.List;

public interface CarService {
    List<Car> getAll(String username);

    void payForProduct(String[] list);
}
