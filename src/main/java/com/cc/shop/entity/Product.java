package com.cc.shop.entity;

public class Product {

    private Integer productId;
    private String productName;
    private String productDecs;
    private String productImg;
    private Double productPrice;
    private Integer productRepertory;
    private int priority;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDecs() {
        return productDecs;
    }

    public void setProductDecs(String productDecs) {
        this.productDecs = productDecs;
    }

    public String getProductImg() {
        return productImg;
    }

    public void setProductImg(String productImg) {
        this.productImg = productImg;
    }

    public Double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Double productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getProductRepertory() {
        return productRepertory;
    }

    public void setProductRepertory(Integer productRepertory) {
        this.productRepertory = productRepertory;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
