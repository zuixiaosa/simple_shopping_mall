package com.cc.shop.web;

import com.cc.shop.dto.Result;
import com.cc.shop.entity.Car;
import com.cc.shop.entity.User;
import com.cc.shop.service.CarService;
import com.cc.shop.util.CheckLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/car")
public class CarController {

    @Autowired
    private CarService carService;
    CheckLogin checkLogin = new CheckLogin();

//    获取购物车信息
    @RequestMapping(value = "/getCar" ,method = RequestMethod.GET)
    @ResponseBody
    public Result<List> getCart(HttpSession session){

        List<Car> list = null;
        if (!checkLogin.checkLogin(session))
            return new Result<List>(203,"请用户先登录！");
        try {
            User user = (User) session.getAttribute("user");
            list = carService.getAll(user.getUsername());
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Result<List>("购物车的信息",list);
    }

//    结算接口
//    这里还有问题，就是结算的时候没检查和减去库存
    @RequestMapping(value = "/payment",method = RequestMethod.POST)
    @ResponseBody
    public Result<String> payment(@RequestBody String [] list,HttpSession session){
        if (!checkLogin.checkLogin(session))
            return new Result<>(203,"请用户先登录！");
        carService.payForProduct(list);
        return new Result<String>(200,"购买成功！");
    }

}
