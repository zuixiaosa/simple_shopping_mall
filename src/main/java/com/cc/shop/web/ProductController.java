package com.cc.shop.web;

import com.cc.shop.dto.Result;
import com.cc.shop.entity.*;
import com.cc.shop.service.ProductService;
import com.cc.shop.util.CheckLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

//模糊查询，日后数据量大了之后要不要加入分页！！
    @RequestMapping(value = "/findByVague",method = RequestMethod.POST)
    @ResponseBody
    public Result<List> getVague(@RequestBody Param param){

        List<Product> list = null;
        try {
            list = productService.findByVague(param.getParam());
        }catch (Exception e){
            e.printStackTrace();
        }
        if (list.size()==0){
            return new Result<>(201,"找不到要查找的商品");
        }
        return new Result<List>("查找的商品如下",list);
    }

//获取商品详细信息
    @RequestMapping(value = "/productDetail",method = RequestMethod.POST)
    @ResponseBody
    public Result<List> getDetail(@RequestBody Param param){
        List<Product> list = null;
        try {
            list = productService.findByName(param.getParam());

        }catch (Exception e){
            e.printStackTrace();
        }
        if (list.size()==0){
            return new Result<>(201,"找不到要查找的商品");
        }
        return new Result<List>("商品详情",list);
    }

//加入购物车
    @RequestMapping(value = "/insertCar",method = RequestMethod.POST)
    @ResponseBody
    public Result<String> insertCar(@RequestBody Car car, HttpSession session){
        User user = (User) session.getAttribute("user");
        CheckLogin checkLogin = new CheckLogin();
        if (!checkLogin.checkLogin(session))
            return new Result<String>(203,"请用户先进行登录操作");
        Car myCar = productService.checkInCar(car.getProductName(),user.getUsername());
        if (myCar==null){
//            System.out.println(car.getProductName());
//            System.out.println(car.getProductPrice());
//            System.out.println(car.getProductImg());
//            System.out.println(car.getProductNum());
//            System.out.println(user.getUsername());
            productService.insertCar(car.getProductName(),car.getProductPrice(),car.getProductImg(),car.getProductNum(),
                   user.getUsername());
            return new Result<String>(200,"加入购物车成功");
        }
        else {
//            System.out.println(car.getProductName());
//            System.out.println(car.getProductPrice());
//            System.out.println(car.getProductImg());
//            System.out.println(car.getProductNum());
//            System.out.println(user.getUsername());
            productService.updateCar(car.getProductName(),car.getProductNum(),user.getUsername());
            return new Result<String>(200,"加入购物车成功");
        }
    }

//    要进行分页处理
    @RequestMapping(value = "/getAllProduct/{page}",method = RequestMethod.GET)
    @ResponseBody
    public Result<List> getAllProduct(@PathVariable("page") Integer page){
        List<Product> list = null;
        if (page==null)
            page = 1;
        int limit = 3;
        try {
            list = productService.getAllProduct(page,limit);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Result<List>("获取的商品为如下！",list);
    }

//  获取总页数
    @RequestMapping(value = "/getTotalProduct",method = RequestMethod.GET)
    @ResponseBody
    public Result<Integer> getTotalProduct(){
        int n = productService.getTotalProduct();
        return new Result<>(200,"返回成功！",n);
    }

    @Autowired
    private TaskExecutor executor;

    //    购买接口
    @RequestMapping(value = "/productBuy",method = RequestMethod.POST)
    @ResponseBody
    public Result<String> buyProduct(@RequestBody Buy buy, HttpSession session){
        CheckLogin checkLogin = new CheckLogin();

        if (!checkLogin.checkLogin(session)){
            return new Result<>(203,"用户还没登录！");
        }
        Boolean b ;
        b = productService.buyProduct(buy.getProductName(),buy.getNum());
        // 以后加一个购买记录表，在已有的表上加一个状态
//        User user = (User) session.getAttribute("user");
//        executor.execute(new Runnable() {
//            @Override
//            public void run() {
//                synchronized (productService){
//
//                }
//            }
//        });
        System.out.println(b);
        if (b)
            return new Result<>(200,"购买成功！");
        else
            return new Result<>(201,"购买失败！");
    }

}
