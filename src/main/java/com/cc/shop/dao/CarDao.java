package com.cc.shop.dao;

import com.cc.shop.entity.Car;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CarDao {
    List<Car> getAllByName(@Param("username") String username);

    void delMore(String [] list);
}
