package com.cc.shop.dao;

import com.cc.shop.entity.Car;
import com.cc.shop.entity.Product;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductDao {

    List<Product> findByVague(@Param("param") String param);

    List<Product> findByName(@Param("param") String param);

    void insertCar(@Param("productName")String productName, @Param("productPrice")Double productPrice,
                   @Param("productImg") String productImg,@Param("num") int num,@Param("username") String username);

    Car checkInCar(@Param("productName") String productName,@Param("username")String username);

    Integer getNum(@Param("productName") String productName,@Param("username")String username);

    void updateCar(@Param("productName")String productName, @Param("num")int num,@Param("username")String username);

    int getTotalProduct();

    List<Product> getProductByPage(@Param("page") int page, @Param("limit")int limit);

    int getProduct(@Param("productName") String productName);

    void upDateProductNum(@Param("now")int now,@Param("productName") String productName);
}
