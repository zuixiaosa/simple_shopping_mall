package com.cc.shop.dao;

import com.cc.shop.entity.User;
import org.apache.ibatis.annotations.Param;

public interface UserDao {

    User findByName(@Param("username")String  username);

    User checkByUsername(@Param("username")String username);

    void addUser(@Param("username")String username, @Param("password")String password);
}
